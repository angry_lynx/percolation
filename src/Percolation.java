

public class Percolation {

    private boolean[][] matrix;       // matrix of sites
    private final int N;              // matrix size N * N
    private WeightedQuickUnionUF unionFind;

    public Percolation(int N) {
        if (N <= 0) {
            throw new IllegalArgumentException("N <= 0");
        }
        this.N = N;
        unionFind = new WeightedQuickUnionUF(N*N);
        matrix = new boolean[N][N];
        for (int i = 0; i < N; i++) {      // init all sites with close state
            for (int j = 0; j < N; j++) {
                matrix[i][j] = false;
            }
        }
    }

    public void open(int i, int j) {
        if (i <= 0 || j <= 0 || i > N || j > N) {
            throw new IndexOutOfBoundsException("i, j is out of bounds");
        }
        if (matrix[i-1][j-1]) { // if site has opened, exit
            return;
        }
        matrix[i-1][j-1] = true; // mark site as opened

        if (j > 1 && isOpen(i, j-1)) {        // left
            unionFind.union(convert2dCoordTo1D(i, j), convert2dCoordTo1D(i, j-1));
        }
        if (j < N && isOpen(i, j+1)) { // right
            unionFind.union(convert2dCoordTo1D(i, j), convert2dCoordTo1D(i, j+1));
        }
        if (i > 1 && isOpen(i-1, j)) { // up
            unionFind.union(convert2dCoordTo1D(i, j), convert2dCoordTo1D(i-1, j));
        }
        if (i < N && isOpen(i+1, j)) { //down
            unionFind.union(convert2dCoordTo1D(i, j), convert2dCoordTo1D(i+1, j));
        }
    }

    public boolean isOpen(int i, int j) {
        if (i <= 0 || j <= 0 || i > N || j > N) {
            throw new IndexOutOfBoundsException("i, j is out of bounds");
        }
        return matrix[i-1][j-1];
    }


    public boolean isFull(int i, int j) {
        if (i <= 0 || j <= 0 || i > N || j > N) {
            throw new IndexOutOfBoundsException("i, j is out of bounds");
        }
        for (int k = 0; k < N; k++) {
            if (isOpen(1, k+1) && unionFind.connected(convert2dCoordTo1D(i, j), k)) {
                return true;
            }
        }
        return false;
}

    public boolean percolates() {
        for (int i = 0; i < N; i++) {
            if (isFull(N, N - i)) {
                return true;
            }
        }
        return false;
    }

    private int convert2dCoordTo1D(int i, int j) {
        return N * (i-1) + (j-1);
    }

    public static void main(String[] args) {
    }
}














