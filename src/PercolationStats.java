import java.util.Random;


public class PercolationStats {

    private final double mean;
    private final double stddev;
    private final double confidenceLo, confidenceHi;

    public PercolationStats(int N, int T) {
        if (N <= 0 || T <= 0) {
            throw new IllegalArgumentException();
        }

        final int size = N*N;
        Percolation p;
        double[] thresholds = new double[T];
        Random r = new Random(System.currentTimeMillis());
        int[][] indexes = new int[size][2];
        // create array of indexes
        int k = 0;
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                indexes[k][0] = i + 1;
                indexes[k][1] = j + 1;
                k++;
            }
        }

        for (int testLoopCounter = 0; testLoopCounter < T; testLoopCounter++) {
            // shuffle array of indexes
            for (int i = 1; i < size; i++) {
                swap(indexes, i, r.nextInt(i));
            }
            k = 0;
            p = new Percolation(N);
            while (!p.percolates()) {
                p.open(indexes[k][0], indexes[k][1]);
                k++;
            }
            thresholds[testLoopCounter] = k / (size * 1.0);
        }

        double accumulator = 0;
        for (double t : thresholds) {
            accumulator += t;
        }
        mean = accumulator / T;

        accumulator = 0;
        for (double t : thresholds) {
            accumulator += ((t - mean)*(t-mean));
        }
        stddev = Math.sqrt(accumulator/(T-1));

        confidenceLo = mean - (1.96*stddev/Math.sqrt(T));
        confidenceHi = mean + (1.96*stddev/Math.sqrt(T));
    }

    public double mean() {
        return mean;
    }

    public double stddev() {
        return stddev;
    }

    public double confidenceLo() {
        return confidenceLo;
    }

    public double confidenceHi() {
        return confidenceHi;
    }

    private void swap(int[][] a, int i, int j) {
        int[] temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }

    public static void main(String[] args) {
        int N = Integer.parseInt(args[0]);
        int T = Integer.parseInt(args[1]);

        Stopwatch sw = new Stopwatch();

        PercolationStats percol = new PercolationStats(N, T);

        double t = sw.elapsedTime();

        // 95% confidence interval of mean estimation.
        double lower =
                percol.mean() - 1.96 * Math.sqrt(percol.stddev()) / Math.sqrt(T);
        double upper =
                percol.mean() + 1.96 * Math.sqrt(percol.stddev()) / Math.sqrt(T);

        System.out.println("time used               = " + t + "s");
        System.out.println("mean                    = " + percol.mean());
        System.out.println("stddev                  = " + percol.stddev());
        System.out.println("95% confidence interval = " + lower + "," + upper);
    }
}